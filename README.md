[![coverage report](https://gitlab.com/sahil.kabra/user-details/badges/master/coverage.svg)](https://gitlab.com/sahil.kabra/user-details/-/commits/master)

# Spring boot sample to create, edit and update user details
This project is a sample Spring boot implementation that allows a user to create, edit and update user details.
It does a very basic in memory user authentication with username and password hardcoded.
CSRF has also been disabled as this is a demo.

There are only two users:
- `user` having role `user`. They have read-only access to the service.
- `admin` having role `admin`. They have read-write access to the service.

All details are stored in an in-memory H2 database that is lost when the project is shut down.

Tables are created on startup using flyway. This will also insert a few records into the tables. These scripts can be
found in `src/main/resources/db/migration/V1.1__loadInitialData.sql`

Entry/Exit of every method is logged at the debug level using the `demo.user.core.LoggingAspect`.

A sample circuit breaker has been implemented for the method `demo.user.api.UserDetailService.find()`. This has been implemented using `resilience4j`.

When the database operation throws an exception, the service will return an empty user details object with only the id attribute populated.

The User id and Address ids for the GET requests can only be numeric.


## Quickly get started

### Pre-requisites
- JDK 8 installed

### Steps
- Run `./gradlew bootRun` to start a local server on port 8080

#### Create User
```
curl -u admin:password -X POST -H "Content-Type: application/json" \
  -d '{"firstName": "Joe", "lastName": "Finch", "gender": "male", "title": "Mr", "addresses": [{"street": "some street", "city": "a city", "state": "a state", "postcode": 2000}]}' \
  http://localhost:8080/users -v
```

#### Get User
```
curl -u user:password -X GET http://localhost:8080/users/1
```

#### Update User Details
```
curl -u admin:password -X PATCH  -H "Content-Type: application/json" \
  -d '{"firstName": "Joe"}' \
  http://localhost:8080/users/1 -v
```

#### Update User Address
```
curl -u admin:password -X PATCH  -H "Content-Type: application/json" \
  -d '{"city": "new city"}' \
  http://localhost:8080/users/1/address/1 -v
```

### Commands
- Run `./gradlew clean test` to run tests
- Run `./gradlew clean pactVerify` to create and verify pacts. This will create and verify the pacts. Pact tests are located in `src/test/pacts`
- Run `./gradlew clean build` to generate an executable spring boot jar file
- Run `./gradlew bootRun` to start a local http server on port 8080
