package demo.user.pact;

import au.com.dius.pact.consumer.MockServer;
import au.com.dius.pact.consumer.dsl.DslPart;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.consumer.junit5.PactConsumerTestExt;
import au.com.dius.pact.consumer.junit5.PactTestFor;
import au.com.dius.pact.core.model.RequestResponsePact;
import au.com.dius.pact.core.model.annotations.Pact;
import io.pactfoundation.consumer.dsl.LambdaDsl;
import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static demo.user.UserDetailObjectMother.createValidUserJson;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(PactConsumerTestExt.class)
public class PactUserDetailsConsumerTest {

    @Pact(provider = "UserDetailsProvider", consumer = "testConsumer")
    public RequestResponsePact validUserId(PactDslWithProvider builder) {
        return builder
                .given("valid user id")
                .uponReceiving("Get user details")
                .path("/users/1")
                .method("GET")
                .willRespondWith()
                .status(200)
                .body(getUserDetailBody())
                .toPact();
    }

    @Test
    @PactTestFor(pactMethod = "validUserId")
    public void testValidUserId(MockServer mockServer) throws Exception {
        HttpResponse response = Request.Get(mockServer.getUrl() + "/users/1")
                .execute().returnResponse();

        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(200);
    }

    @Pact(provider = "UserDetailsProvider", consumer = "testConsumer")
    public RequestResponsePact invalidUserId(PactDslWithProvider builder) {
        return builder
                .given("invalid user id")
                .uponReceiving("Get user details")
                .path("/users/abc")
                .method("GET")
                .willRespondWith()
                .status(422)
                .toPact();
    }

    @Test
    @PactTestFor(pactMethod = "invalidUserId")
    public void testInvalidUserId(MockServer mockServer) throws Exception {
        HttpResponse response = Request.Get(mockServer.getUrl() + "/users/abc")
                .execute().returnResponse();

        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(422);
    }

    @Pact(provider = "UserDetailsProvider", consumer = "testConsumer")
    public RequestResponsePact createUserDetails(PactDslWithProvider builder) {
        return builder
                .given("create user details")
                .uponReceiving("POST user details")
                .path("/users")
                .method("POST")
                .body(createValidUserJson())
                .willRespondWith()
                .status(201)
                .body(getUserDetailBody())
                .toPact();
    }

    @Test
    @PactTestFor(pactMethod = "createUserDetails")
    public void testCreateUser(MockServer mockServer) throws Exception {
        HttpResponse response = Request.Post(mockServer.getUrl() + "/users")
                .bodyString(createValidUserJson(), ContentType.APPLICATION_JSON)
                .execute().returnResponse();

        assertThat(response.getStatusLine().getStatusCode()).isEqualTo(201);
    }

    private DslPart getUserDetailBody() {
        return LambdaDsl.newJsonBody(o -> {
            o.numberType("id");
            o.stringType("title", "Mr");
            o.stringType("firstName", "Name");
            o.stringType("lastName", "Name");
            o.stringType("gender", "Male");
            o.minArrayLike("addresses", 1, address -> {
                address.numberType("id");
                address.stringType("street");
                address.stringType("city");
                address.stringType("state");
                address.numberType("postcode");
            });
        }).build();
    }
}
