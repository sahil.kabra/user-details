package demo.user.pact;

import au.com.dius.pact.provider.junit5.HttpTestTarget;
import au.com.dius.pact.provider.junit5.PactVerificationContext;
import au.com.dius.pact.provider.junit5.PactVerificationInvocationContextProvider;
import au.com.dius.pact.provider.junitsupport.Consumer;
import au.com.dius.pact.provider.junitsupport.Provider;
import au.com.dius.pact.provider.junitsupport.State;
import au.com.dius.pact.provider.junitsupport.loader.PactFolder;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestTemplate;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@Provider("UserDetailsProvider")
@Consumer("testConsumer")
@SpringBootTest(webEnvironment = RANDOM_PORT)
@PactFolder("pacts")
@ActiveProfiles("pacts")
public class PactUserDetailsProviderTest {

    @LocalServerPort
    private int port;

    @BeforeAll
    static void beforeAll() {
        System.setProperty("pact.verifier.publishResults", "false");
    }

    @BeforeEach
    public void beforeEach(PactVerificationContext context) {
        context.setTarget(new HttpTestTarget("localhost", port));
    }

    @TestTemplate
    @ExtendWith(PactVerificationInvocationContextProvider.class)
    void pactVerificationTestTemplate(PactVerificationContext context) {
        context.verifyInteraction();
    }

    @State("valid user id")
    public void validUserIdProvider() {
    }

    @State("invalid user id")
    public void invalidUserIdProvider() {
    }

    @State("create user details")
    public void createUserDetailsProvider() {
    }
}
