package demo.user;

import demo.user.api.Address;
import demo.user.api.UserDetail;

import java.util.ArrayList;
import java.util.List;

import static demo.user.AddressObjectMother.createValidAddress;

public class UserDetailObjectMother {
    public static String createValidUserJson() {
        return String.join(
                System.getProperty("line.separator"),
                "{",
                    "\"title\": \"mr\",",
                    "\"firstName\": \"first\",",
                    "\"lastName\": \"last\",",
                    "\"gender\": \"male\",",
                    "\"addresses\": [{",
                        "\"street\": \"12345 mock street\",",
                        "\"city\": \"Sydney\",",
                        "\"state\": \"nsw\",",
                        "\"postcode\": 2000",
                    "}]",
                "}"
        );
    }

    public static UserDetail createValidUserDetail() {
        return createValidUserDetail(1);
    }
    public static UserDetail createValidUserDetail(int numAddresses) {
        List<Address> addresses = new ArrayList<>(numAddresses);

        for (int i = 0; i < numAddresses; i++) {
            addresses.add(createValidAddress());
        }

        return UserDetail.builder()
                .firstName("first")
                .lastName("last")
                .gender("male")
                .title("title")
                .addresses(addresses)
                .build();
    }

    private UserDetailObjectMother() {}
}
