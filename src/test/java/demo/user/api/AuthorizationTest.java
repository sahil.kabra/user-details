package demo.user.api;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import static demo.user.UserDetailObjectMother.createValidUserJson;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class AuthorizationTest extends BaseTest {

    @Test
    public void shouldCreateUserWhenRoleIsAdmin() throws Exception {
        mockMvc.perform(post("/users")
                .with(user("user").roles("ADMIN"))
                .contentType("application/json")
                .content(createValidUserJson())
        ).andExpect(status().isCreated());
    }

    @Test
    public void shouldReturnForbiddenWhenCreatingUserWithUserRole() throws Exception {
        mockMvc.perform(post("/users")
                .with(user("user").roles("USER"))
                .contentType("application/json")
                .content(createValidUserJson())
        ).andExpect(status().isForbidden());
    }

    @Test
    public void shouldUpdateUserWhenRoleIsAdmin() throws Exception {
        mockMvc.perform(
                patch("/users/1")
                        .with(user("user").roles("ADMIN"))
                        .contentType("application/json")
                        .content("{\"firstName\": \"updated\"}")
        ).andExpect(status().isOk());
    }

    @Test
    public void shouldReturnForbiddenWhenUpdatingUserWithUserRole() throws Exception {
        mockMvc.perform(
                patch("/users/1")
                        .with(user("user").roles("USER"))
                        .contentType("application/json")
                        .content("{\"firstName\": \"updated\"}")
        ).andExpect(status().isForbidden());
    }

    @ParameterizedTest
    @ValueSource(strings = {"USER","ADMIN"})
    public void shouldAllowReadAccessForAllRoles(String role) throws Exception {
        mockMvc.perform(
                get("/users/1")
                        .with(user("user").roles(role))
        ).andExpect(status().isOk());
    }

    @Test
    public void shouldReturnForbiddenForInvalidRole() throws Exception {
        mockMvc.perform(
                get("/users/1")
                        .with(user("user").roles("INVALID"))
        ).andExpect(status().isForbidden());
    }

    @Test
    public void shouldReturnUnauthorizedWhenNoUserDetailsPassed() throws Exception {
        mockMvc.perform(
                get("/users/1")
        ).andExpect(status().isUnauthorized());
    }
}
