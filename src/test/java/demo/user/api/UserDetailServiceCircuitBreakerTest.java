package demo.user.api;

import demo.user.persistence.UserDetailEntity;
import demo.user.persistence.UserDetailsRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.annotation.DirtiesContext.ClassMode.AFTER_CLASS;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
@DirtiesContext(classMode = AFTER_CLASS)
public class UserDetailServiceCircuitBreakerTest {

    @MockBean
    private UserDetailsRepository userDetailsRepository;

    @Autowired
    private UserDetailService service;

    @Test
    public void shouldCallFallbackMethodWhenDbThrowsException() {
        when(userDetailsRepository.findById(anyLong())).thenThrow(RuntimeException.class);

        Optional<UserDetail> detail = service.find(42L);

        assertThat(detail).isPresent();
        assertThat(detail.get().getId()).isEqualTo(42L);
        assertThat(detail.get().getFirstName()).isNull();
    }

    @Test
    public void shouldReturnActualValueWhenNoException() {
        when(userDetailsRepository.findById(anyLong()))
                .thenReturn(Optional.of(UserDetailEntity.builder().id(42L).firstName("actual value").build()));

        Optional<UserDetail> detail = service.find(42L);

        assertThat(detail).isPresent();
        assertThat(detail.get().getId()).isEqualTo(42L);
        assertThat(detail.get().getFirstName()).isEqualTo("actual value");
    }
}
