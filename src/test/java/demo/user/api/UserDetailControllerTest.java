package demo.user.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import demo.user.core.Mapper;
import demo.user.persistence.AddressEntity;
import demo.user.persistence.AddressRepository;
import demo.user.persistence.UserDetailEntity;
import demo.user.persistence.UserDetailsRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.Comparator;
import java.util.Optional;

import static demo.user.UserDetailObjectMother.createValidUserDetail;
import static demo.user.UserDetailObjectMother.createValidUserJson;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class UserDetailControllerTest extends BaseTest {

    @Autowired
    private UserDetailsRepository userDetailsRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private ObjectMapper jsonObjectMapper;

    @Autowired
    private Mapper modelMapper;

    @Test
    @WithMockUser(roles = "ADMIN")
    public void shouldCreateUser_WhenPostValidUserDetails() throws Exception {
        int userRecordsBeforePost = userDetailsRepository.findAll().size();

        mockMvc.perform(post("/users")
                .contentType("application/json")
                .content(createValidUserJson())
        ).andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", greaterThan(0)))
                .andReturn().getResponse().getContentAsString();

        int userRecordsAfterPost = userDetailsRepository.findAll().size();

        assertThat(userRecordsAfterPost).isEqualTo(userRecordsBeforePost + 1);
    }

    @Test
    @WithMockUser(roles = "USER")
    public void shouldGetUserWhenGetValidUserId() throws Exception {
        UserDetailEntity expected = setupTestUser();

        UserDetail actual = jsonObjectMapper.readValue(
                mockMvc.perform(
                        get("/users/" + expected.getId())
                )
                        .andExpect(status().isOk())
                        .andReturn().getResponse().getContentAsString(),
                UserDetail.class);

        assertThat(actual).isEqualToIgnoringGivenFields(expected, "addresses");
        assertThat(actual.getAddresses()).hasSameSizeAs(expected.getAddresses());
        assertThat(actual.getAddresses().get(0)).isEqualToComparingFieldByField(expected.getAddresses().get(0));
    }

    @Test
    @WithMockUser(roles = "USER")
    public void shouldReturnNotFoundWhenInvalidUserId() throws Exception {
        long invalidUserId = getInvalidUserId();

        mockMvc.perform(
                get("/users/" + invalidUserId)
        ).andExpect(status().isNotFound());

        assertThat(userDetailsRepository.findById(invalidUserId)).isNotPresent();
    }

    @Test
    @WithMockUser(roles = "USER")
    public void shouldGetAllAddressesOfUser() throws Exception {
        int expectedAddressCount = 4;
        UserDetailEntity createdUser = setupTestUser(expectedAddressCount);

        UserDetail actual = jsonObjectMapper.readValue(
                mockMvc.perform(
                        get("/users/" + createdUser.getId())
                ).andExpect(status().isOk())
                        .andReturn().getResponse().getContentAsString(),
                UserDetail.class);

        assertThat(actual.getAddresses()).hasSize(expectedAddressCount);
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void shouldBeAbleToUpdateFirstName() throws Exception {
        UserDetailEntity createdUser = setupTestUser();
        String expected = "updated";

        mockMvc.perform(
                patch("/users/" + createdUser.getId())
                        .contentType("application/json")
                        .content("{\"firstName\": \"" + expected + "\"}")
        ).andExpect(status().isOk());

        Optional<UserDetailEntity> actual = userDetailsRepository.findById(createdUser.getId());

        assertThat(actual).isPresent();
        assertThat(actual.get().getFirstName()).isEqualTo(expected);
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void shouldReturnNotFoundWhenUpdateForInvalidUser() throws Exception {
        long invalidUserId = getInvalidUserId();
        mockMvc.perform(
                patch("/users/" + invalidUserId)
                        .contentType("application/json")
                        .content("{\"firstName\": \"does not matter\"}")
        ).andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void shouldReturnNotFoundWhenUpdateForInvalidAddress() throws Exception {
        long invalidAddressId = getInvalidAddressId();
        mockMvc.perform(
                patch("/users/1/address/" + invalidAddressId)
                        .contentType("application/json")
                        .content("{\"firstName\": \"does not matter\"}")
        ).andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void shouldBeAbleToPatchStreet() throws Exception {
        UserDetailEntity createdUser = setupTestUser();
        String expected = "updated";
        Long addressId = createdUser.getAddresses().get(0).getId();

        mockMvc.perform(
                patch("/users/" + createdUser.getId() + "/address/" + addressId)
                        .contentType("application/json")
                        .content("{\"street\": \"" + expected + "\"}")
        ).andExpect(status().isOk());

        Optional<AddressEntity> actual = addressRepository.findById(addressId);

        assertThat(actual).isPresent();
        assertThat(actual.get().getStreet()).isEqualTo(expected);
    }

    @Test
    @WithMockUser(roles = "USER")
    public void shouldErrorWhenUserIdPassedAsString() throws Exception {
        mockMvc.perform(
                get("/users/abc")
        ).andExpect(status().is(422));
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void shouldErrorWhenAddressIdPassedAsString() throws Exception {
        mockMvc.perform(
                patch("/users/1/address/abc")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content("{\"city\": \"moore\"}")
        ).andExpect(status().is(422));
    }

    private UserDetailEntity setupTestUser() {
        return setupTestUser(1);
    }

    private UserDetailEntity setupTestUser(Integer numAddresses) {
        return userDetailsRepository.save(
                modelMapper.map(createValidUserDetail(numAddresses), UserDetailEntity.class)
        );
    }

    private long getInvalidUserId() {
        long maxUserId = userDetailsRepository.findAll().stream()
                .max(Comparator.comparing(UserDetailEntity::getId))
                .get()
                .getId();

        return maxUserId + 10;
    }

    private long getInvalidAddressId() {
        long maxUserId = addressRepository.findAll().stream()
                .max(Comparator.comparing(AddressEntity::getId))
                .get()
                .getId();

        return maxUserId + 10;
    }
}
