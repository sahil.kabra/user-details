package demo.user.api;

import demo.user.core.Mapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;

import javax.transaction.Transactional;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static demo.user.UserDetailObjectMother.createValidUserDetail;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.context.annotation.FilterType.ASSIGNABLE_TYPE;

@DataJpaTest(
        includeFilters = @ComponentScan.Filter(type = ASSIGNABLE_TYPE, classes = {UserDetailService.class, Mapper.class})
)
class UserDetailServiceTest {

    @Autowired
    private UserDetailService service;

    @Test
    @Transactional
    public void shouldCreateNewRecord() {
        UserDetail mockUserDetail = createValidUserDetail();
        UserDetail saved = service.save(mockUserDetail);

        assertThat(saved.getId()).isNotNull();
        assertThat(saved).isEqualToIgnoringGivenFields(mockUserDetail, "id", "addresses");
        assertThat(saved.getAddresses()).hasSameSizeAs(mockUserDetail.getAddresses());
        assertThat(saved.getAddresses().get(0).getId()).isNotNull();
        assertThat(saved.getAddresses().get(0)).isEqualToIgnoringGivenFields(mockUserDetail.getAddresses().get(0), "id");
    }

    @Test
    public void shouldUpdateUserDetails() {
        String expected = "updated data";
        UserDetail mockUserDetail = service.save(createValidUserDetail());

        service.update(mockUserDetail, Collections.singletonMap("firstName", expected));

        UserDetail updated = service.find(mockUserDetail.getId()).get();

        assertThat(updated.getFirstName()).isEqualTo(expected);
    }

    @Test
    public void shouldNotUpdateIdWhenUpdatingUserDetails() {
        String expected = "updated data";
        UserDetail mockUserDetail = service.save(createValidUserDetail());
        Map<String, String> updates = new HashMap<>();
        updates.put("firstName", expected);
        updates.put("id", "invalid");

        service.update(mockUserDetail, updates);

        UserDetail updated = service.find(mockUserDetail.getId()).get();

        assertThat(updated.getId()).isEqualTo(mockUserDetail.getId());
        assertThat(updated.getFirstName()).isEqualTo(expected);
    }

    @Test
    public void shouldReturnUserForValidUserId() {
        Long expected = service.save(createValidUserDetail()).getId();

        Optional<UserDetail> actual = service.find(expected);

        assertThat(actual).isPresent();
        assertThat(actual.get().getId()).isEqualTo(expected);
    }

    @Test
    public void shouldReturnEmptyForInvalidUserId() {
        Optional<UserDetail> actual = service.find(-99L);

        assertThat(actual).isNotPresent();
    }

    @Test
    public void shouldReturnAddressForValidUserIdAndAddress() {
        UserDetail expected = service.save(createValidUserDetail());

        Optional<Address> actual = service.findAddress(expected.getId(), expected.getAddresses().get(0).getId());

        assertThat(actual).isPresent();
        assertThat(actual.get().getId()).isEqualTo(expected.getAddresses().get(0).getId());
    }

    @Test
    public void shouldReturnEmptyForInvalidUserIdWhenFindingAddress() {
        Optional<Address> actual = service.findAddress(-99L, 1L);

        assertThat(actual).isNotPresent();
    }

    @Test
    public void shouldReturnEmptyForInvalidAddressIdWhenFindingAddress() {
        Optional<Address> actual = service.findAddress(1L, -99L);

        assertThat(actual).isNotPresent();
    }
}
