package demo.user.core;

import demo.user.api.UserDetail;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static demo.user.UserDetailObjectMother.createValidUserDetail;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class PatchUtilsTest {

    @Test
    public void shouldPatchProvidedField() {
        Map<String, String> updates = new HashMap<>();
        updates.put("lastName", "modified now");

        UserDetail user = createValidUserDetail();

        UserDetail updated = PatchUtils.applyPatch(user, updates);

        assertThat(updated.getLastName()).isEqualTo("modified now");
    }

    @Test
    public void shouldNotErrorWhenFieldNotFound() {
        Map<String, String> updates = new HashMap<>();
        updates.put("lastName1", "modified now");

        UserDetail user = createValidUserDetail();

        PatchUtils.applyPatch(user, updates);
    }
}
