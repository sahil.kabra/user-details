package demo.user;

import demo.user.api.Address;

public class AddressObjectMother {

    public static Address createValidAddress() {
        return Address.builder()
                .street("1234 George Street")
                .city("Sydney")
                .state("NSW")
                .postcode(2000)
                .build();
    }

    private AddressObjectMother(){}
}
