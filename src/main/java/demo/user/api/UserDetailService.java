package demo.user.api;

import demo.user.core.Mapper;
import demo.user.core.PatchUtils;
import demo.user.persistence.AddressEntity;
import demo.user.persistence.AddressRepository;
import demo.user.persistence.UserDetailEntity;
import demo.user.persistence.UserDetailsRepository;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Map;
import java.util.Optional;

@Service
@Slf4j
public class UserDetailService {

    private final UserDetailsRepository userDetailsRepository;
    private final Mapper mapper;
    private final AddressRepository addressRepository;

    public UserDetailService(UserDetailsRepository userDetailsRepository, AddressRepository addressRepository, Mapper mapper) {
        this.userDetailsRepository = userDetailsRepository;
        this.addressRepository = addressRepository;
        this.mapper = mapper;
    }

    @Transactional
    public UserDetail save(UserDetail userDetail) {
        try {
            return mapper.map(
                    userDetailsRepository.save(mapper.map(userDetail, UserDetailEntity.class)),
                    UserDetail.class
            );
        } catch (Exception e) {
            log.error("Error while saving user details", e);
            throw e;
        }
    }

    @CircuitBreaker(name = "findUserById", fallbackMethod = "findFallback")
    public Optional<UserDetail> find(Long userId) {
        try {
            return userDetailsRepository.findById(userId).map(u ->
                    mapper.map(u, UserDetail.class)
            );
        } catch (Exception e) {
            log.error("Error for findUserById for {}", userId, e);
            throw e;
        }
    }

    public Optional<UserDetail> findFallback(Long userId, Exception e) {
        // return an empty object in case of failure
        return Optional.of(UserDetail.builder().id(userId).build());
    }

    @Transactional
    public UserDetail update(UserDetail user, Map<String, String> updates) {
        updates.remove("id");
        UserDetail patched = PatchUtils.applyPatch(user, updates);
        return save(patched);
    }

    @Transactional
    public Address updateAddress(Address address, Map<String, String> updates) {
        updates.remove("id");
        Address patched = PatchUtils.applyPatch(address, updates);
        return saveAddress(patched);
    }

    public Optional<Address> findAddress(Long userId, Long addressId) {
        return userDetailsRepository.findById(userId).flatMap(u ->
                addressRepository.findOneByUserAndId(u, addressId)
        ).map(a -> mapper.map(a, Address.class));
    }

    private Address saveAddress(Address address) {
        return mapper.map(
                addressRepository.save(mapper.map(address, AddressEntity.class)),
                Address.class
        );
    }
}
