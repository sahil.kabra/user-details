package demo.user.api;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDetail {
    private Long id;
    private String title;
    private String firstName;
    private String lastName;
    private String gender;

    private List<Address> addresses;
}
