package demo.user.api;

import demo.user.core.Number;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.ConstraintViolationException;
import java.util.Map;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.PATCH;

@RestController
@RequestMapping(value = "/users")
@Validated
@Slf4j
public class UserDetailsController {

    private final UserDetailService service;

    public UserDetailsController(UserDetailService service) {
        this.service = service;
    }

    @PostMapping
    @ResponseStatus(CREATED)
    public UserDetail createUser(@RequestBody UserDetail userDetail) {
        return service.save(userDetail);
    }

    @RequestMapping(value = "/{userId}", method = GET)
    public ResponseEntity<UserDetail> getUserDetail(@PathVariable @Number String userId) {
        return service.find(Long.parseLong(userId)).map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @RequestMapping(value = "/{userId}", method = PATCH)
    public ResponseEntity<UserDetail> patchUserDetail(@PathVariable @Number String userId, @RequestBody Map<String, String> updates) {
        return service.find(Long.parseLong(userId))
                .map(u -> service.update(u, updates))
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @RequestMapping(value = "/{userId}/address/{addressId}", method = PATCH)
    public ResponseEntity<Address> patchAddress(@PathVariable @Number String userId,
                                                @PathVariable @Number String addressId,
                                                @RequestBody Map<String, String> updates
    ) {
        return service.findAddress(Long.parseLong(userId), Long.parseLong(addressId))
                .map(address -> service.updateAddress(address, updates))
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<String> handleException(ConstraintViolationException e) {

        return new ResponseEntity<>(
                e.getMessage(),
                UNPROCESSABLE_ENTITY
        );
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> handleException(Exception e) {
        // generic fall back error handler to log error details and return a generic message
        log.error("Encounter an error during operation", e);

        return new ResponseEntity<>("Server error", INTERNAL_SERVER_ERROR);
    }
}
