package demo.user.core;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.Pattern;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target(ElementType.PARAMETER)
@Retention(RUNTIME)
@Pattern(regexp = "\\d+", message = "should be a number")
@Constraint(validatedBy = {})
public @interface Number {

    String message() default "should be a number";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
