package demo.user.core;

import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.Map;

@Slf4j
public class PatchUtils {
    public static <T> T applyPatch(T object, Map<String, String> updates) {
        updates.forEach((key, value) -> {
            Field f = ReflectionUtils.findField(object.getClass(), key);
            if (f != null) {
                f.setAccessible(true);
                ReflectionUtils.setField(f, object, value);
            } else {
                log.warn("field {} not found to update", key);
            }
        });
        return object;
    }
}
