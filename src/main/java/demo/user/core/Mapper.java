package demo.user.core;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class Mapper {
    private final ModelMapper mapper = new ModelMapper();

    public <S, T> T map(S source, Class<T> destinationType) {
        return mapper.map(source, destinationType);
    }
}
