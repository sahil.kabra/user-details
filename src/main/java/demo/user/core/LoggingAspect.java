package demo.user.core;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Component
@Aspect
@Slf4j
public class LoggingAspect {

    @Before("execution(* demo.user..*(..))")
    public void logEntry(JoinPoint jp) {
        log.debug("Entering method {}", jp.getSignature());
    }

    @After("execution(* demo.user..*(..))")
    public void logExit(JoinPoint jp) {
        log.debug("Exiting method {}", jp.getSignature());
    }
}
