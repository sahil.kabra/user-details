create table user (
  id identity not null,
  title varchar(255),
  first_name varchar(255),
  last_name varchar(255),
  gender varchar(20)
);

create table address(
  id identity not null,
  user_id bigint,
  street varchar(255),
  city varchar(255),
  state varchar(255),
  postcode int,
  foreign key (user_id) references user(id)
);
