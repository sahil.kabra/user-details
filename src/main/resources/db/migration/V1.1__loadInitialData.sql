insert into user (id, title, first_name, last_name, gender)
values (1, 'Mr', 'John', 'Smith', 'male');

insert into address(id, user_id, street, city, state, postcode)
values (1, 1, '12 London Street', 'Blacktown', 'NSW', 2148);

insert into address(id, user_id, street, city, state, postcode)
values (2, 1, '5 Bank Street', 'Meadowbank', 'NSW', 2142);


insert into user (id, title, first_name, last_name, gender)
values (2, 'Mrs', 'Julie', 'Smith', 'female');

insert into address(id, user_id, street, city, state, postcode)
values (3, 2, '9 Clarence Avenue', 'Sydney', 'NSW', 2000);


insert into user (id, title, first_name, last_name, gender)
values (3, 'Dr', 'Albert', 'Joseph', 'male');

insert into address(id, user_id, street, city, state, postcode)
values (4, 3, '200 Murray Street', 'Perth', 'WA', 6000);


insert into user (id, title, first_name, last_name, gender)
values (4, 'Miss', 'Ann', 'Varughese', 'female');

insert into address(id, user_id, street, city, state, postcode)
values (5, 4, '96 Park Lane', 'Darlinghurst', 'NSW', 2000);


insert into user (id, title, first_name, last_name, gender)
values (5, 'Mr', 'Don', 'Corleone', 'male');

insert into address(id, user_id, street, city, state, postcode)
values (6, 5, '20 Hunter Street', 'Botany Bay', 'NSW', 2112);

insert into address(id, user_id, street, city, state, postcode)
values (7, 5, '16 Mort Road', 'Mortdate', 'NSW', 2120);
